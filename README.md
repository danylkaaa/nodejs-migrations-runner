# migrations-runner

[![pipeline status](https://gitlab.com/ZulusK/nodejs-migrations-runner/badges/master/pipeline.svg)](https://gitlab.com/ZulusK/nodejs-migrations-runner/commits/master)
[![coverage report](https://gitlab.com/ZulusK/nodejs-migrations-runner/badges/master/coverage.svg)](https://gitlab.com/ZulusK/nodejs-migrations-runner/commits/master)

Applied migrations, that stored in a dir, using Knex query builder.

```bash
npm i @zulus/migrations-runner
```

### Structure

- [API](#api)
- [CLI usage](#cli-usage)
- [Contributing rules](#contributing)

<a name="cli-usage"></a>

### USAGE
# in console
```bash
./node_modules/.bin/migrations_runner -d ./src/migrations
```

<a name="api"></a>

### API
### `runMigrations(knex, dirname, [options])`
Applied migrations, stored at dirname in files, with name in format`<timestamp>-<name>.sql`
* `knex` - The Knex instance
* `dirname` -The absolute path to directory with migrations
* `options`
    * `src` - list of migrations, that should be used
    * `last` - last date of migrations

### CLI
Write next lines in your console, to view documentation for CLI tool
```bash
runMigrations --help
```


<a name="contributing"></a>

### Contributing

To start contributing do

```bash
git clone git@gitlab.com:ZulusK/nodejs-migrations-runner.git
git checkout develop
git checkout -b <your-branch-name>
```

The project is developed in accordance with the [GitFlow][gitflow-docs] methodology.

##### What it means

1. All work you should do in your own **local** branch (naming is important, look below), then make pull request to **develop** branch
2. Your local branch should not have conflicts with repository **develop** branch. To avoid it, before push to repository, do:
   ```bash
   git pull origin develop
   # resolve all conflicts, if they exists
   git add --all
   git commit -m "fix conflicts"
   git push origin <your-branch-name>
   ```
3. We use next naming of branches:

| branch template                      | description                                                 |
| ------------------------------------ | ----------------------------------------------------------- |
| `feat/<short-feature-name>`          | new feature, ex. `feat-add-logger`                          |
| `fix/<short-fix-name>`               | fix of existing feature, ex. `fix-logger`                   |
| `refactor/<short-scope-description>` | refactor, linting, style changes, ex. `style-update-eslint` |
| `test/<short-scope-descriptiopn>`    | tests, ex. `test-db-connections`                            |
| `docs/<short-scope-descriptiopn>`    | documentation, ex. `test-db-connections`                    |

##### Important, before push

1. We use **eslint** with this [rules][eslint-rules] to lint code, before making pull
   request, lint your code:
   ```bash
   npm run lint
   ```
2. Before making pull request, run tests

   ```bash
   npm run test
   ```

[gitflow-docs]: https://gitversion.readthedocs.io/en/latest/git-branching-strategies/gitflow-examples/
[eslint-rules]: .eslintrc
