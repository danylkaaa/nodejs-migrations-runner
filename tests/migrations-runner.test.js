const chai = require('chai');
const sinon = require('sinon');
const path = require('path');
const migrationRunner = require('../lib/migrations-runner');

chai.use(require('chai-sorted'));

const { expect } = chai;

function getKnexMock() {
  return {
    raw: sinon.spy()
  };
}

describe('migrations-runner', () => {
  describe('getListOfNeededMigrations', () => {
    it('should return sorted list of names of files in a directory', async () => {
      const list = await migrationRunner.getListOfNeededMigrations(path.join(__dirname, 'resources/suite1'));
      expect(list).to.be.an('array').and.to.have.length(3);
      expect(list).to.include.all.members(
        ['123-initial.sql', '124-second.sql', '224-third.sql']
          .map(v => path.join(__dirname, 'resources/suite1', v))
      );
      expect(list).to.be.sorted();
    });
    it('should error ENOENT: no such file or directory, if directory is invalid', (done) => {
      migrationRunner.getListOfNeededMigrations(path.join(__dirname, 'resources/suite0')).then(() => {
        done(new Error('Expect an error, but nothing was happen'));
      }).catch((err) => {
        expect(err.message).to.match(/^ENOENT: no such file or directory/);
        done();
      });
    });
    it('should return empty list, if directory is empty', async () => {
      const list = await migrationRunner.getListOfNeededMigrations(path.join(__dirname, 'resources/suite2'));
      expect(list).to.have.length(0);
    });
    it('should read only .sql files in a dir', async () => {
      const list = await migrationRunner.getListOfNeededMigrations(path.join(__dirname, 'resources/suite3'));
      expect(list).to.have.length(2);
      expect(list).to.not.include([path.join(__dirname, 'resources/suite3/invalid.txt')]);
    });
  });
  describe('runMigrations', () => {
    it('should apply migrations only from specified files in right order', async () => {
      const knexMock = getKnexMock();
      await migrationRunner.runMigrations(knexMock, path.join(__dirname, 'resources/suite1'), {
        src: ['123-initial.sql', '224-third.sql']
      });
      expect(knexMock.raw.calledTwice).to.be.eq(true);
      expect(knexMock.raw.args[0][0]).to.be.eq('CREATE TABLE test(KEY INT)\n');
      expect(knexMock.raw.args[1][0]).to.be.eq('INSERT INTO test VALUES (2)\n');
    });
    it('should apply migrations only from specified files in right order', async () => {
      const knexMock = getKnexMock();
      await migrationRunner.runMigrations(knexMock, path.join(__dirname, 'resources/suite1'), {
        src: ['224-third.sql', '123-initial.sql']
      });
      expect(knexMock.raw.calledTwice).to.be.eq(true);
      expect(knexMock.raw.args[0][0]).to.be.eq('CREATE TABLE test(KEY INT)\n');
      expect(knexMock.raw.args[1][0]).to.be.eq('INSERT INTO test VALUES (2)\n');
    });
    it('should apply only after last migration', async () => {
      const knexMock = getKnexMock();
      await migrationRunner.runMigrations(knexMock, path.join(__dirname, 'resources/suite1'), {
        last: 123
      });
      expect(knexMock.raw.calledTwice).to.be.eq(true);
      expect(knexMock.raw.args[0][0]).to.be.eq('INSERT INTO test VALUES (1)\n');
      expect(knexMock.raw.args[1][0]).to.be.eq('INSERT INTO test VALUES (2)\n');
    });
    it('should apply migrations step by step ', async () => {
      const knexMock = getKnexMock();
      await migrationRunner.runMigrations(knexMock, path.join(__dirname, 'resources/suite1'));
      expect(knexMock.raw.calledThrice).to.be.eq(true);
      expect(knexMock.raw.args[0][0]).to.be.eq('CREATE TABLE test(KEY INT)\n');
      expect(knexMock.raw.args[1][0]).to.be.eq('INSERT INTO test VALUES (1)\n');
      expect(knexMock.raw.args[2][0]).to.be.eq('INSERT INTO test VALUES (2)\n');
    });
    it('should error ENOENT: no such file or directory, if directory is invalid', (done) => {
      const knexMock = getKnexMock();
      migrationRunner.runMigrations(knexMock, path.join(__dirname, 'resources/suite0')).then(() => {
        done(new Error('Expect an error, but nothing was happen'));
      }).catch((err) => {
        expect(err.message).to.match(/^Directory .* is not exists$/);
        done();
      });
    });
  });
});
