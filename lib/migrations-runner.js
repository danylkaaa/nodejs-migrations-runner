const fse = require('fs-extra');
const path = require('path');
const _ = require('lodash');

async function getListOfNeededMigrations(dirname, options = {}) {
  let files = await fse.readdir(dirname);
  files = files.filter(file => file.endsWith('.sql'));
  if (options.src !== undefined) {
    files = _.filter(files, file => options.src.indexOf(file) >= 0);
  }
  const MIGRATION_FILES_PATTERN = /(\d*)/;
  files = _.filter(files, (file) => {
    const matched = MIGRATION_FILES_PATTERN.exec(file);
    const migrationTimestamp = matched ? matched[1] : 0;
    return !options.last || options.last < migrationTimestamp;
  });
  return files.sort().map(file => path.join(dirname, file));
}

async function readAllFiles(files) {
  return Promise.all(files.map(file => fse.readFile(file)));
}

async function runMigrations(knex, dirname, options = {}) {
  if (!path.isAbsolute(dirname)) throw new Error(`${dirname} is not an absolute path`);
  if (!await fse.pathExists(dirname)) {
    throw new Error(`Directory ${dirname} is not exists`);
  }
  const sortedListOfMigrationsFiles = await getListOfNeededMigrations(dirname, options);
  console.log('Detected migration files:');
  sortedListOfMigrationsFiles.forEach(file => console.log(file));
  const filesContent = await readAllFiles(sortedListOfMigrationsFiles);
  console.log('Apply migrations');
  for (let i = 0; i < filesContent.length; i++) {
    console.log('Apply migration from file %s', sortedListOfMigrationsFiles[i]);
    // eslint-disable-next-line no-await-in-loop
    await knex.raw(filesContent[i].toString('UTF8'));
  }
}

module.exports = {
  runMigrations,
  getListOfNeededMigrations
};
