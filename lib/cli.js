const { runMigrations } = require('./migrations-runner');
const path = require('path');
const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const Knex = require('knex');

const optionDefinitions = [
  {
    name: 'help',
    type: Boolean,
    description: 'Display help information'
  },
  {
    name: 'database',
    type: String,
    description: 'Database name',
  },
  {
    name: 'host',
    alias: 'h',
    default: 'localhost',
    type: String,
    typeLabel: '{underline string}',
    description: 'Database host'
  },
  {
    name: 'user',
    alias: 'u',
    required: true,
    type: String,
    typeLabel: '{underline string}',
    description: 'Database user'
  },
  {
    name: 'password',
    alias: 'p',
    required: true,
    type: String,
    typeLabel: '{underline string}',
    description: 'Database password'
  },
  {
    name: 'port',
    alias: 'P',
    required: true,
    type: String,
    typeLabel: '{underline number}',
    description: 'Database port'
  },
  {
    name: 'client',
    alias: 'c',
    required: true,
    type: String,
    typeLabel: '{underline string}',
    description: 'Database client [pg, mysql, mysql2]'
  },
  {
    name: 'last',
    alias: 'l',
    type: Number,
    description: 'Last used migration time'
  },
  {
    name: 'dir',
    alias: 'd',
    type: String,
    typeLabel: '{underline string}',
    description: 'Directory with migrations'
  },
  {
    name: 'src',
    alias: 's',
    type: String,
    multiple: true,
    defaultOption: true,
    description: 'List of migrations, should be applied'
  }
];
const sections = [
  {
    header: 'SQL migrations',
    content: 'Run SQL migrations, stored in specified folder'
  },
  {
    header: 'Synopsis',
    content: 'runMigrations \\{-d /path/to/migrations\\} -h localhost -p 5432 -c pg [...]'
  },
  {
    header: 'Options',
    optionList: optionDefinitions,
  }
];

const options = commandLineArgs(optionDefinitions);

if (options.help) {
  console.log(commandLineUsage(sections));
} else {
  if (options.dir === undefined) {
    console.error('Parameter --dir is required');
    process.exit(1);
  }
  const knex = Knex({
    client: options.client,
    connection: {
      host: options.host,
      port: options.port,
      user: options.user,
      password: options.password,
      database: options.database
    }
  });
  runMigrations(knex, path.join(process.cwd(), options.dir), options).then(() => {
    console.log('done');
    knex.destroy();
  });
}
